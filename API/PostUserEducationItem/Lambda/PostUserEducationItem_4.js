var AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({
    "region": process.env.AwsRegion
});
const TableName = process.env.TableName;

exports.handler = (event, context, callback) => {

    var EventEmitter = require('events').EventEmitter;
    var wf = new EventEmitter();

    if (event.userID.length == 0) {
        context.fail(JSON.stringify({
            "data":null,
            "error": {
                "code": 400,
                "message": "Missing parameters /userID/",
                "type": "Missing parameters",
                "should_display_error": "false"
            },
            "statusCode": 400
        }));
        return;
    }
    wf.user_sub_id = event.userID;

    function Trim(string) {
        string = string.replace(/ {1,}/g," ");
        return string.trim();
    }

    function TrimArrayElements(array){
        for(var i=0;i<array.length;i++){
            array[i] = Trim(array[i]);
        }
        return array;
    }

    wf.once('check_request_body', function() {
        var errors = [];
        var missingParameters = '';
        if(!event.body.hasOwnProperty("DegreeName")){
            errors.push("DegreeName");
            missingParameters = '/'+missingParameters+'DegreeName/';
        }
        if(!event.body.hasOwnProperty("CurrentFlag")){
            if(errors.length>0){
                missingParameters = missingParameters+'CurrentFlag/';
            } else {
                missingParameters = '/'+missingParameters+'CurrentFlag/';
            }
            errors.push("CurrentFlag");

        }
        if(!event.body.hasOwnProperty('SchoolName')){
            if(errors.length>0){
                missingParameters = missingParameters+'SchoolName/';
            } else {
                missingParameters = '/'+missingParameters+'SchoolName/';
            }
            errors.push("SchoolName");
        }
        if(!event.body.hasOwnProperty('SpecialAchievements')){
            if(errors.length>0){
                missingParameters = missingParameters+'SpecialAchievements/';
            } else {
                missingParameters = '/'+missingParameters+'SpecialAchievements/';
            }
            errors.push("SpecialAchievements");
        }
        if(!event.body.hasOwnProperty('SpecializationField')){
            if(errors.length>0){
                missingParameters = missingParameters+'SpecializationField/';
            } else {
                missingParameters = '/'+missingParameters+'SpecializationField/';
            }
            errors.push("SpecializationField");
        }
        if(!event.body.hasOwnProperty('LocationCity')){
            if(errors.length>0){
                missingParameters = missingParameters+'LocationCity/';
            } else {
                missingParameters = '/'+missingParameters+'LocationCity/';
            }
            errors.push("LocationCity");
        }
        if(!event.body.hasOwnProperty('LocationCountry')){
            if(errors.length>0){
                missingParameters = missingParameters+'LocationCountry/';
            } else {
                missingParameters = '/'+missingParameters+'LocationCountry/';
            }
            errors.push("LocationCountry");
        }
        if(!event.body.hasOwnProperty('UniversityName')){
            if(errors.length>0){
                missingParameters = missingParameters+'UniversityName/';
            } else {
                missingParameters = '/'+missingParameters+'UniversityName/';
            }
            errors.push("UniversityName");
        }
        if(!event.body.hasOwnProperty('CourseDateFrom')){
            if(errors.length>0){
                missingParameters = missingParameters+'CourseDateFrom/';
            } else {
                missingParameters = '/'+missingParameters+'CourseDateFrom/';
            }
            errors.push("CourseDateFrom");
        }
        if(!event.body.hasOwnProperty('CourseDateTo')){
            if(!event.body.hasOwnProperty("CurrentFlag")){
                if(errors.length>0){
                    missingParameters = missingParameters+'CourseDateTo/';
                } else {
                    missingParameters = '/'+missingParameters+'CourseDateTo/';
                }
                errors.push("CourseDateTo");
            } else {
                if(event.body.CurrentFlag != 'Yes'){
                    // nothing to do
                    if(errors.length>0){
                        missingParameters = missingParameters+'CourseDateTo/';
                    } else {
                        missingParameters = '/'+missingParameters+'CourseDateTo/';
                    }
                    errors.push("CourseDateTo");
                }
            }
        }
        if(errors.length > 0){
            var message = 'Missing parameters '+missingParameters;
            context.fail(JSON.stringify({
                "data":null,
                "error": {
                    "code": 400,
                    "message": message,
                    "type": "Missing parameters",
                    "should_display_error": "false"
                },
                "statusCode": 400
            }));
            return;
        } else {
            wf.emit('check_request_by_field_level');

        }
    });
    wf.once('check_request_by_field_level',function(){
        var DegreeName,CurrentFlag,Description,SchoolName,SpecialAchievements,SpecializationField,LocationCity,LocationCountry,CourseDateFrom,CourseDateTo,EducationLinks,UniversityName;
        DegreeName = event.body.DegreeName;
        CurrentFlag = event.body.CurrentFlag;
        SchoolName = event.body.SchoolName;
        SpecialAchievements = event.body.SpecialAchievements;
        SpecializationField = event.body.SpecializationField;
        LocationCountry = event.body.LocationCountry;
        LocationCity = event.body.LocationCity;
        CourseDateFrom = event.body.CourseDateFrom;
        UniversityName = event.body.UniversityName;

        var errors = [];
        var invalidParameters = '';
        if(DegreeName.length == 0){
            errors.push("DegreeName");
            invalidParameters = '/'+invalidParameters+'DegreeName/';
        }
        if(SchoolName.length == 0){
            if(errors.length>0){
                invalidParameters = invalidParameters+'SchoolName/';
            } else {
                invalidParameters = '/'+invalidParameters+'SchoolName/';
            }
            errors.push("SchoolName");

        }
        if(SpecialAchievements.length == 0){
            if(errors.length>0){
                invalidParameters = invalidParameters+'SpecialAchievements/';
            } else {
                invalidParameters = '/'+invalidParameters+'SpecialAchievements/';
            }
            errors.push("SpecialAchievements");

        }
        if(SpecializationField.length == 0){
            if(errors.length>0){
                invalidParameters = invalidParameters+'SpecializationField/';
            } else {
                invalidParameters = '/'+invalidParameters+'SpecializationField/';
            }
            errors.push("SpecializationField");

        }
        if(LocationCity.length == 0){
            if(errors.length>0){
                invalidParameters = invalidParameters+'LocationCity/';
            } else {
                invalidParameters = '/'+invalidParameters+'LocationCity/';
            }
            errors.push("LocationCity");

        }
        if(LocationCountry.length == 0){
            if(errors.length>0){
                invalidParameters = invalidParameters+'LocationCountry/';
            } else {
                invalidParameters = '/'+invalidParameters+'LocationCountry/';
            }
            errors.push("LocationCountry");

        }
        if(CourseDateFrom.length == 0){
            if(errors.length>0){
                invalidParameters = invalidParameters+'CourseDateFrom/';
            } else {
                invalidParameters = '/'+invalidParameters+'CourseDateFrom/';
            }
            errors.push("CourseDateFrom");

        }
        if(CurrentFlag != 'Yes' && CurrentFlag != 'No'){
            if(errors.length>0){
                invalidParameters = invalidParameters+'CurrentFlag/';
            } else {
                invalidParameters = '/'+invalidParameters+'CurrentFlag/';
            }
            errors.push("CurrentFlag");

        }
        if(errors.length > 0){
            var message = 'Invalid parameters '+invalidParameters;
            context.fail(JSON.stringify({
                "data":null,
                "error": {
                    "code": 400,
                    "message": message,
                    "type": "Invalid parameters",
                    "should_display_error": "false"
                },
                "statusCode": 400
            }));
            return;
        } else {
            if(CurrentFlag == 'Yes'){
                wf.emit('get_user_item_count');
            } else {
                if(!event.body.hasOwnProperty('CourseDateTo')){
                    context.fail(JSON.stringify({
                        "data":null,
                        "error": {
                            "code": 400,
                            "message": "Missing parameters /CourseDateTo/",
                            "type": "Missing parameters",
                            "should_display_error": "false"
                        },
                        "statusCode": 400
                    }));
                    return;
                } else {
                    if(event.body.CourseDateTo.length == 0){
                        context.fail(JSON.stringify({
                            "data":null,
                            "error": {
                                "code": 400,
                                "message": "Invalid parameters /CourseDateTo/",
                                "type": "Invalid parameters",
                                "should_display_error": "false"
                            },
                            "statusCode": 400
                        }));
                        return;
                    }
                    wf.emit('get_user_item_count');
                }
            }


        }
    })

    wf.once('get_user_item_count', function (){
        var params = {
            TableName: TableName,
            KeyConditionExpression: "#uid = :userId",
            ExpressionAttributeNames: {
                "#uid": "UserID"
            },
            ExpressionAttributeValues: {
                ":userId": wf.user_sub_id
            },
            ProjectionExpression: ["ItemID"]
        };

        docClient.query(params, function(err, data) {
            if (err) {
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                        "code": 500,
                        "message": "Internal server error",
                        "type": "Server Error",
                        "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                if(data.Items.length == 0){
                    wf.ItemID = 1;
                    wf.emit('store_data_to_user_education_data_to_table');
                } else {
                    var maxItem = Math.max.apply(Math, data.Items.map(function(o) { return o.ItemID; }));
                    wf.ItemID = maxItem+1;
                    wf.emit('store_data_to_user_education_data_to_table');
                }
            }
        });
    })

    wf.once('store_data_to_user_education_data_to_table',function(){
        var Description,CourseDateTo,EducationLinks;
        var Created_Timestamp = new Date().toISOString();
        if(event.body.hasOwnProperty('Description')){
            if(event.body.Description.length == 0){
                // nothing to do
                Description = '';
            } else {
                Description = Trim(event.body.Description);
            }
        }
        if(event.body.hasOwnProperty('CourseDateTo')){
            if(event.body.CurrentFlag == 'Yes'){
                CourseDateTo = ''
            } else {
                if(event.body.CourseDateTo.length == 0){
                    // nothing to do
                    CourseDateTo = '';
                } else {
                    CourseDateTo = event.body.CourseDateTo;
                }
            }

        }
        if(event.body.hasOwnProperty('EducationLinks')){
            if(event.body.EducationLinks.length == 0){
                // nothing to do
                EducationLinks = [];
            } else {
                EducationLinks = TrimArrayElements(event.body.EducationLinks);
            }
        }
        wf.user_exp_item = {
            "UserID": wf.user_sub_id,
            "ItemID": wf.ItemID,
            "DegreeName": Trim(event.body.DegreeName),
            "CurrentFlag": event.body.CurrentFlag,
            "Description": Description,
            "SchoolName": Trim(event.body.SchoolName),
            "SpecialAchievements": Trim(event.body.SpecialAchievements),
            "SpecializationField":Trim(event.body.SpecializationField),
            "LocationCity": Trim(event.body.LocationCity),
            "LocationCountry": Trim(event.body.LocationCountry),
            "Status": "Active",
            "CourseDateFrom":event.body.CourseDateFrom,
            "EducationLinks":EducationLinks,
            "CourseDateTo":CourseDateTo,
            "UniversityName":Trim(event.body.UniversityName),
            "TSCreated": Created_Timestamp,
            "TSUpdated": Created_Timestamp
        };
        if(event.body.hasOwnProperty('Description')){
            if(event.body.Description.length == 0){
                // nothing to do
                delete wf.user_exp_item["Description"];
            }
        } else {
            delete wf.user_exp_item["Description"];
        }
        if(event.body.hasOwnProperty('CourseDateTo')){
            if(event.body.CurrentFlag == 'Yes'){
                delete wf.user_exp_item["CourseDateTo"];
            } else {
                if(event.body.CourseDateTo.length == 0){
                    // nothing to do
                    delete wf.user_exp_item["CourseDateTo"];
                }
            }
        } else {
            delete wf.user_exp_item["CourseDateTo"];
        }
        if(event.body.hasOwnProperty('EducationLinks')){
            if(event.body.EducationLinks.length == 0){
                // nothing to do
                delete wf.user_exp_item["EducationLinks"];
            }
        } else {
            delete wf.user_exp_item["EducationLinks"];
        }
        var params = {
            Item: wf.user_exp_item,
            TableName: TableName
        };
        docClient.put(params, function(err, data) {
            if (err) {
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                        "code": 500,
                        "message": "Internal server error",
                        "type": "Server Error",
                        "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                if(!wf.user_exp_item["Description"]){
                    wf.user_exp_item["Description"] ="";
                }
                if(!wf.user_exp_item["CourseDateTo"]){
                    wf.user_exp_item["CourseDateTo"] ="";
                }
                if(!wf.user_exp_item["EducationLinks"]){
                    wf.user_exp_item["EducationLinks"] = [];
                }
                context.done(null,{
                    "data":{
                        "MainData": wf.user_exp_item
                    },
                    "error": null,
                    "statusCode": 201
                });
                return;
            }
        });
    })
    wf.emit('check_request_body');
};

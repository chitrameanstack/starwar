var AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({
    "region": process.env.AwsRegion
});
const TableName = process.env.TableName;

exports.handler = (event, context, callback) => {

    var EventEmitter = require('events').EventEmitter;
    var wf = new EventEmitter();

    if (event.userID.length == 0) {
        context.fail(JSON.stringify({
            "data":null,
            "error": {
                "code": 400,
                "message": "Missing parameters /userID/",
                "type": "Missing parameters",
                "should_display_error": "false"
            },
            "statusCode": 400
        }));
        return;
    }
    wf.user_sub_id = event.userID;

    function Trim(string) {
        string = string.replace(/ {1,}/g," ");
        return string.trim();
    }

    function TrimArrayElements(array){
        for(var i=0;i<array.length;i++){
            array[i] = Trim(array[i]);
        }
        return array;
    }

    wf.once('check_request_body', function() {
        var errors = [];
        var missingParameters = '';
        if (!event.body.hasOwnProperty("AwardName")) {
            errors.push("AwardName");
            missingParameters = '/' + missingParameters + 'AwardName/';
        }
        if (errors.length > 0) {
            var message = 'Missing parameters ' + missingParameters;
            context.fail(JSON.stringify({
                "data": null,
                "error": {
                    "code": 400,
                    "message": message,
                    "type": "Missing parameters",
                    "should_display_error": "false"
                },
                "statusCode": 400
            }));
            return;
        } else {
            wf.emit('check_request_by_field_level');

        }
    });
    wf.once('check_request_by_field_level', function() {
        var AwardName;
        AwardName = event.body.AwardName;

        var errors = [];
        var invalidParameters = '';
        if (AwardName.length == 0) {
            errors.push("AwardName");
            invalidParameters = '/' + invalidParameters + 'AwardName/';
        }
        if (errors.length > 0) {
            var message = 'Invalid parameters ' + invalidParameters;
            context.fail(JSON.stringify({
                "data": null,
                "error": {
                    "code": 400,
                    "message": message,
                    "type": "Invalid parameters",
                    "should_display_error": "false"
                },
                "statusCode": 400
            }));
            return;
        } else {
            wf.emit('get_user_item_count');

        }
    })

    wf.once('get_user_item_count', function (){
        var params = {
            TableName: TableName,
            KeyConditionExpression: "#uid = :userId",
            ExpressionAttributeNames: {
                "#uid": "UserID"
            },
            ExpressionAttributeValues: {
                ":userId": wf.user_sub_id
            },
            ProjectionExpression: ["ItemID"]
        };

        docClient.query(params, function(err, data) {
            if (err) {
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                        "code": 500,
                        "message": "Internal server error",
                        "type": "Server Error",
                        "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                if(data.Items.length == 0){
                    wf.ItemID = 1;
                    wf.emit('store_data_to_user_award_to_table');
                } else {
                    var maxItem = Math.max.apply(Math, data.Items.map(function(o) { return o.ItemID; }));
                    wf.ItemID = maxItem+1;
                    wf.emit('store_data_to_user_award_to_table');
                }
            }
        });
    })

    wf.once('store_data_to_user_award_to_table', function() {
        var Description, AwardLinks, AwardIssuer, AwardName, AwardDate, AwardURL;
        var Created_Timestamp = new Date().toISOString();
        if (event.body.hasOwnProperty('Description')) {
            if (event.body.Description.length == 0) {
                // nothing to do
                Description = '';
            } else {
                Description = Trim(event.body.Description);
            }
        }
        if (event.body.hasOwnProperty('AwardIssuer')) {
            if (event.body.AwardIssuer.length == 0) {
                // nothing to do
                AwardIssuer = '';
            } else {
                AwardIssuer = Trim(event.body.AwardIssuer);
            }
        }
        if (event.body.hasOwnProperty('AwardDate')) {
            if (event.body.AwardDate.length == 0) {
                // nothing to do
                AwardDate = '';
            } else {
                AwardDate = Trim(event.body.AwardDate);
            }
        }
        if (event.body.hasOwnProperty('AwardURL')) {
            if (event.body.AwardURL.length == 0) {
                // nothing to do
                AwardURL = '';
            } else {
                AwardURL = Trim(event.body.AwardURL);
            }
        }
        if (event.body.hasOwnProperty('AwardLinks')) {
            if (!Array.isArray(event.body.AwardLinks) || event.body.AwardLinks.length == 0) {
                // nothing to do
                AwardLinks = [];
            } else {
                AwardLinks = TrimArrayElements(event.body.AwardLinks);
            }
        }
        wf.user_award_item = {
            "UserID": wf.user_sub_id,
            "ItemID": wf.ItemID,
            "AwardDate": AwardDate,
            "Status": 'Active',
            "AwardIssuer": AwardIssuer,
            "Description": Description,
            "AwardName": Trim(event.body.AwardName),
            "AwardURL": AwardURL,
            "AwardLinks": AwardLinks,
            "TSCreated": Created_Timestamp,
            "TSUpdated": Created_Timestamp
        };
        if (event.body.hasOwnProperty('Description')) {
            if (event.body.Description.length == 0) {
                // nothing to do
                delete wf.user_award_item["Description"];
            }
        } else {
            delete wf.user_award_item["Description"];
        }
        if (event.body.hasOwnProperty('AwardIssuer')) {
            if (event.body.AwardIssuer.length == 0) {
                // nothing to do
                delete wf.user_award_item["AwardIssuer"];
            }
        } else {
            delete wf.user_award_item["AwardIssuer"];
        }
        if (event.body.hasOwnProperty('AwardDate')) {
            if (event.body.AwardDate.length == 0) {
                // nothing to do
                delete wf.user_award_item["AwardDate"];
            }
        } else {
            delete wf.user_award_item["AwardDate"];
        }
        if (event.body.hasOwnProperty('AwardURL')) {
            if (event.body.AwardURL.length == 0) {
                // nothing to do
                delete wf.user_award_item["AwardURL"];
            }
        } else {
            delete wf.user_award_item["AwardURL"];
        }
        if (event.body.hasOwnProperty('AwardLinks')) {
            if (!Array.isArray(event.body.AwardLinks) || event.body.AwardLinks.length == 0) {
                // nothing to do
                delete wf.user_award_item["AwardLinks"];
            }
        } else {
            delete wf.user_award_item["AwardLinks"];
        }
        var params = {
            Item: wf.user_award_item,
            TableName: TableName
        };
        docClient.put(params, function(err, data) {
            if (err) {
                context.fail(JSON.stringify({
                    "data": null,
                    "error": {
                        "code": 500,
                        "message": "Internal server error",
                        "type": "Server Error",
                        "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                if(!wf.user_award_item["Description"]){
                    wf.user_award_item["Description"] ="";
                }
                if(!wf.user_award_item["AwardIssuer"]){
                    wf.user_award_item["AwardIssuer"] ="";
                }
                if(!wf.user_award_item["AwardDate"]){
                    wf.user_award_item["AwardDate"] ="";
                }
                if(!wf.user_award_item["AwardURL"]){
                    wf.user_award_item["AwardURL"] ="";
                }
                if(!wf.user_award_item["AwardLinks"]){
                    wf.user_award_item["AwardLinks"] = [];
                }
                context.done(null, {
                    "data":{
                        "MainData": wf.user_award_item
                    },
                    "error": null,
                    "statusCode": 201
                });
                return;
            }

        });


    })

    wf.emit('check_request_body');
};
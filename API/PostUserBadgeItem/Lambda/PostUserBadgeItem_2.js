var AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({
    "region": process.env.AwsRegion
});
const TableName = process.env.TableName;

exports.handler = (event, context, callback) => {

    var EventEmitter = require('events').EventEmitter;
    var wf = new EventEmitter();

    if (event.userID.length == 0) {
        context.fail(JSON.stringify({
            "data":null,
            "error": {
              "code": 400,
              "message": "Missing parameters /userID/",
              "type": "Missing parameters",
              "should_display_error": "false"
            },
            "statusCode": 400
        }));
        return;
    }
    wf.user_sub_id = event.userID;

    function Trim(string) {
        string = string.replace(/ {1,}/g," ");
        return string.trim();
    }

    function TrimArrayElements(array){
        for(var i=0;i<array.length;i++){
            array[i] = Trim(array[i]);
        }
        return array;
    }

    wf.once('check_request_body', function() {
        var errors = [];
        var missingParameters = '';
        if(!event.body.hasOwnProperty("BadgeName")){
            errors.push("BadgeName");
            missingParameters = '/'+missingParameters+'BadgeName/';
        }
        if(errors.length > 0){
            var message = 'Missing parameters '+missingParameters;
            context.fail(JSON.stringify({
                "data":null,
                "error": {
                  "code": 400,
                  "message": message,
                  "type": "Missing parameters",
                  "should_display_error": "false"
                },
                "statusCode": 400
            }));
            return;
        } else {
            wf.emit('check_request_by_field_level');
             
        }
    });
    wf.once('check_request_by_field_level',function(){
      var BadgeName, BadgeExpiryFlag, BadgeExpiryDate;
      BadgeName = event.body.BadgeName;

      var errors = [];
      var invalidParameters = '';
      if(BadgeName.length == 0){
          errors.push("BadgeName");
          invalidParameters = '/'+invalidParameters+'BadgeName/';
      }
      if(event.body.hasOwnProperty("BadgeExpiryFlag")){
          BadgeExpiryFlag = event.body.BadgeExpiryFlag;
          if(BadgeExpiryFlag != 'Yes' && BadgeExpiryFlag != 'No' && BadgeExpiryFlag.length != 0){
            if(errors.length>0){
                invalidParameters = invalidParameters+'BadgeExpiryFlag/';
            } else {
                invalidParameters = '/'+invalidParameters+'BadgeExpiryFlag/';
            }
            errors.push("BadgeExpiryFlag");    
          }
      }
      if(errors.length > 0){
          var message = 'Invalid parameters '+invalidParameters;
          context.fail(JSON.stringify({
              "data":null,
              "error": {
                "code": 400,
                "message": message,
                "type": "Invalid parameters",
                "should_display_error": "false"
              },
              "statusCode": 400
          }));
          return;
      } else {
          if(event.body.hasOwnProperty('BadgeExpiryFlag')){
            if(event.body.BadgeExpiryFlag == 'Yes'){
              if(!event.body.hasOwnProperty('BadgeExpiryDate')){
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                      "code": 400,
                      "message": "Missing parameters /BadgeExpiryDate/",
                      "type": "Missing parameters",
                      "should_display_error": "false"
                    },
                    "statusCode": 400
                }));
                return;
              } else {
                if(event.body.BadgeExpiryDate.length == 0){
                  context.fail(JSON.stringify({
                      "data":null,
                      "error": {
                        "code": 400,
                        "message": "Invalid parameters /BadgeExpiryDate/",
                        "type": "Invalid parameters",
                        "should_display_error": "false"
                      },
                      "statusCode": 400
                  }));
                  return;
                }
                wf.emit('get_user_item_count');
              }
            } else {
              wf.emit('get_user_item_count');
            }
          } else {
            wf.emit('get_user_item_count');
          }
          
           
      }
    })
    
    wf.once('get_user_item_count', function (){
      var params = {
            TableName: TableName,
            KeyConditionExpression: "#uid = :userId",
            ExpressionAttributeNames: {
                "#uid": "UserID"
            },
            ExpressionAttributeValues: {
                ":userId": wf.user_sub_id
            },
            ProjectionExpression: ["ItemID"]
        };

        docClient.query(params, function(err, data) {
            if (err) {
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                      "code": 500,
                      "message": "Internal server error",
                      "type": "Server Error",
                      "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                if(data.Items.length == 0){
                  wf.ItemID = 1;
                  wf.emit('store_data_to_user_badge_table');
                } else {
                  var maxItem = Math.max.apply(Math, data.Items.map(function(o) { return o.ItemID; }));
                  wf.ItemID = maxItem+1;
                  wf.emit('store_data_to_user_badge_table');
                }
            }
        });
    })

    wf.once('store_data_to_user_badge_table',function(){
      var BadgeDate, BadgeExpiryDate, BadgeExpiryFlag, BadgeIssuer, BadgeLinks, BadgeName, BadgeNumber, BadgeURL, Description;
      var Created_Timestamp = new Date().toISOString();
      if(event.body.hasOwnProperty('Description')){
          if(event.body.Description.length == 0){
              // nothing to do
              Description = '';
          } else {          
              Description = Trim(event.body.Description);
          }
      } 
      if(event.body.hasOwnProperty('BadgeDate')){
          if(event.body.BadgeDate.length == 0){
              // nothing to do
              BadgeDate = '';
          } else {          
              BadgeDate = event.body.BadgeDate;
          }
      } 
      if(event.body.hasOwnProperty('BadgeExpiryDate')){
          if(event.body.BadgeExpiryFlag == 'Yes'){
            BadgeExpiryDate = event.body.BadgeExpiryDate;
          } else {
            BadgeExpiryDate = '';
          }
      }
      if(event.body.hasOwnProperty('BadgeLinks')){
          if(event.body.BadgeLinks.length == 0){
              // nothing to do
              BadgeLinks = [];
          } else {          
              BadgeLinks = TrimArrayElements(event.body.BadgeLinks);
          }
      }
      if(event.body.hasOwnProperty('BadgeIssuer')){
          if(event.body.BadgeIssuer.length == 0){
              // nothing to do
              BadgeIssuer = '';
          } else {          
              BadgeIssuer = Trim(event.body.BadgeIssuer);
          }
          
      }
      if(event.body.hasOwnProperty('BadgeNumber')){
          if(event.body.BadgeNumber.length == 0){
              // nothing to do
              BadgeNumber = '';
          } else {          
              BadgeNumber = Trim(event.body.BadgeNumber);
          }
      } 
      if(event.body.hasOwnProperty('BadgeURL')){
          if(event.body.BadgeURL.length == 0){
              // nothing to do
              BadgeURL = '';
          } else {          
              BadgeURL = Trim(event.body.BadgeURL);
          }
      } 
      wf.user_exp_item = {
          "UserID": wf.user_sub_id,
          "ItemID": wf.ItemID,
          "BadgeName": Trim(event.body.BadgeName),
          "BadgeExpiryFlag": event.body.BadgeExpiryFlag,
          "Description": Description,
          "BadgeDate": BadgeDate,
          "Status": "Active",
          "BadgeIssuer":BadgeIssuer,
          "BadgeExpiryDate":BadgeExpiryDate,
          "BadgeLinks":BadgeLinks,
          "BadgeNumber":BadgeNumber,
          "BadgeURL":BadgeURL,
          "TSCreated": Created_Timestamp,
          "TSUpdated": Created_Timestamp
      };
      if(event.body.hasOwnProperty('Description')){
          if(event.body.Description.length == 0){
              // nothing to do
              delete wf.user_exp_item["Description"];
          } 
      } else {
        delete wf.user_exp_item["Description"];
      }
      if(event.body.hasOwnProperty('BadgeDate')){
          if(event.body.BadgeDate.length == 0){
              // nothing to do
              delete wf.user_exp_item["BadgeDate"];
          } 
      } else {
        delete wf.user_exp_item["BadgeDate"];
      }
      if(event.body.hasOwnProperty('BadgeIssuer')){
          if(event.body.BadgeIssuer.length == 0){
              // nothing to do
              delete wf.user_exp_item["BadgeIssuer"];
          } 
      } else {
        delete wf.user_exp_item["BadgeIssuer"];
      }
      if(event.body.hasOwnProperty('BadgeExpiryDate')){
        if(event.body.BadgeExpiryFlag == 'No'){
          delete wf.user_exp_item["BadgeExpiryDate"];
        } else {
          if(event.body.BadgeExpiryFlag != 'Yes'){
            delete wf.user_exp_item["BadgeExpiryDate"];
          }
        }
      } else {
        delete wf.user_exp_item["BadgeExpiryDate"];
      }
      if(event.body.hasOwnProperty('BadgeLinks')){
          if(event.body.BadgeLinks.length == 0){
              // nothing to do
              delete wf.user_exp_item["BadgeLinks"];
          } 
      } else {
        delete wf.user_exp_item["BadgeLinks"];
      }
      if(event.body.hasOwnProperty('BadgeNumber')){
          if(event.body.BadgeNumber.length == 0){
              // nothing to do
              delete wf.user_exp_item["BadgeNumber"];
          } 
      } else {
        delete wf.user_exp_item["BadgeNumber"];
      }
      if(event.body.hasOwnProperty('BadgeURL')){
          if(event.body.BadgeURL.length == 0){
              // nothing to do
              delete wf.user_exp_item["BadgeURL"];
          } 
      } else {
        delete wf.user_exp_item["BadgeURL"];
      }
      var params = {
          Item: wf.user_exp_item,
          TableName: TableName
      };
      docClient.put(params, function(err, data) {
          if (err) {
              context.fail(JSON.stringify({
                  "data":null,
                  "error": {
                    "code": 500,
                    "message": "Internal server error",
                    "type": "Server Error",
                    "should_display_error": "false"
                  },
                  "statusCode": 500
              }));
              return;
          } else {
              if(!wf.user_exp_item["BadgeDate"]){
                  wf.user_exp_item["BadgeDate"] ="";
              }
              if(!wf.user_exp_item["BadgeExpiryFlag"]){
                  wf.user_exp_item["BadgeExpiryFlag"] ="No";
              }
              if(!wf.user_exp_item["BadgeExpiryDate"]){
                  wf.user_exp_item["BadgeExpiryDate"] ="";
              }
              if(!wf.user_exp_item["BadgeIssuer"]){
                  wf.user_exp_item["BadgeIssuer"] ="";
              }
              if(!wf.user_exp_item["BadgeLinks"]){
                  wf.user_exp_item["BadgeLinks"] = [];
              }
              if(!wf.user_exp_item["BadgeNumber"]){
                  wf.user_exp_item["BadgeNumber"] ="";
              }
              if(!wf.user_exp_item["BadgeURL"]){
                  wf.user_exp_item["BadgeURL"] ="";
              }
              if(!wf.user_exp_item["Description"]){
                  wf.user_exp_item["Description"] ="";
              }
              context.done(null,{
                  "data":{
                    "MainData": wf.user_exp_item
                  },
                  "error": null,
                  "statusCode": 201
              });
              return;
          }

      });

      
    })

    wf.emit('check_request_body');
};

var AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({
    "region": process.env.AwsRegion
});
const TableName = process.env.TableName;

exports.handler = (event, context, callback) => {

    var EventEmitter = require('events').EventEmitter;
    var wf = new EventEmitter();

    if (event.userID.length == 0) {
        context.fail(JSON.stringify({
            "data":null,
            "error": {
                "code": 400,
                "message": "Missing parameters /userID/",
                "type": "Missing parameters",
                "should_display_error": "false"
            },
            "statusCode": 400
        }));
        return;
    }
    wf.user_sub_id = event.userID;

    function Trim(string) {
        string = string.replace(/ {1,}/g," ");
        return string.trim();
    }

    function TrimArrayElements(array){
        for(var i=0;i<array.length;i++){
            array[i] = Trim(array[i]);
        }
        return array;
    }

    wf.once('check_request_body', function() {
        var errors = [];
        var missingParameters = '';
        if(!event.body.hasOwnProperty("CertName")){
            errors.push("CertName");
            missingParameters = '/'+missingParameters+'CertName/';
        }
        if(errors.length > 0){
            var message = 'Missing parameters '+missingParameters;
            context.fail(JSON.stringify({
                "data":null,
                "error": {
                    "code": 400,
                    "message": message,
                    "type": "Missing parameters",
                    "should_display_error": "false"
                },
                "statusCode": 400
            }));
            return;
        } else {
            wf.emit('check_request_by_field_level');

        }
    });
    wf.once('check_request_by_field_level',function(){
        var CertName, CertExpiryFlag, CertExpiryDate;
        CertName = event.body.CertName;

        var errors = [];
        var invalidParameters = '';
        if(CertName.length == 0){
            errors.push("CertName");
            invalidParameters = '/'+invalidParameters+'CertName/';
        }
        if(event.body.hasOwnProperty("CertExpiryFlag")){
            CertExpiryFlag = event.body.CertExpiryFlag;
            if(CertExpiryFlag != 'Yes' && CertExpiryFlag != 'No' && CertExpiryFlag.length != 0){
                if(errors.length>0){
                    invalidParameters = invalidParameters+'CertExpiryFlag/';
                } else {
                    invalidParameters = '/'+invalidParameters+'CertExpiryFlag/';
                }
                errors.push("CertExpiryFlag");
            }
        }
        if(errors.length > 0){
            var message = 'Invalid parameters '+invalidParameters;
            context.fail(JSON.stringify({
                "data":null,
                "error": {
                    "code": 400,
                    "message": message,
                    "type": "Invalid parameters",
                    "should_display_error": "false"
                },
                "statusCode": 400
            }));
            return;
        } else {
            if(event.body.hasOwnProperty('CertExpiryFlag')){
                if(event.body.CertExpiryFlag == 'Yes'){
                    if(!event.body.hasOwnProperty('CertExpiryDate')){
                        context.fail(JSON.stringify({
                            "data":null,
                            "error": {
                                "code": 400,
                                "message": "Missing parameters /CertExpiryDate/",
                                "type": "Missing parameters",
                                "should_display_error": "false"
                            },
                            "statusCode": 400
                        }));
                        return;
                    } else {
                        if(event.body.CertExpiryDate.length == 0){
                            context.fail(JSON.stringify({
                                "data":null,
                                "error": {
                                    "code": 400,
                                    "message": "Invalid parameters /CertExpiryDate/",
                                    "type": "Invalid parameters",
                                    "should_display_error": "false"
                                },
                                "statusCode": 400
                            }));
                            return;
                        }
                        wf.emit('get_user_item_count');
                    }
                } else {
                    wf.emit('get_user_item_count');
                }
            } else {
                wf.emit('get_user_item_count');
            }


        }
    })

    wf.once('get_user_item_count', function (){
        var params = {
            TableName: TableName,
            KeyConditionExpression: "#uid = :userId",
            ExpressionAttributeNames: {
                "#uid": "UserID"
            },
            ExpressionAttributeValues: {
                ":userId": wf.user_sub_id
            },
            ProjectionExpression: ["ItemID"]
        };

        docClient.query(params, function(err, data) {
            if (err) {
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                        "code": 500,
                        "message": "Internal server error",
                        "type": "Server Error",
                        "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                if(data.Items.length == 0){
                    wf.ItemID = 1;
                    wf.emit('store_data_to_user_certificate_table');
                } else {
                    var maxItem = Math.max.apply(Math, data.Items.map(function(o) { return o.ItemID; }));
                    wf.ItemID = maxItem+1;
                    wf.emit('store_data_to_user_certificate_table');
                }
            }
        });
    })

    wf.once('store_data_to_user_certificate_table',function(){
        var CertDate, CertExpiryDate, CertExpiryFlag, CertIssuer, CertLinks, CertName, CertNumber, CertURL, Description;
        var Created_Timestamp = new Date().toISOString();
        if(event.body.hasOwnProperty('Description')){
            if(event.body.Description.length == 0){
                // nothing to do
                Description = '';
            } else {
                Description = Trim(event.body.Description);
            }
        }
        if(event.body.hasOwnProperty('CertDate')){
            if(event.body.CertDate.length == 0){
                // nothing to do
                CertDate = '';
            } else {
                CertDate = event.body.CertDate;
            }
        }
        if(event.body.hasOwnProperty('CertExpiryDate')){
            if(event.body.CertExpiryFlag == 'Yes'){
                CertExpiryDate = event.body.CertExpiryDate;
            } else {
                CertExpiryDate = '';
            }
        }
        if(event.body.hasOwnProperty('CertLinks')){
            if(event.body.CertLinks.length == 0){
                // nothing to do
                CertLinks = [];
            } else {
                CertLinks = TrimArrayElements(event.body.CertLinks);
            }
        }
        if(event.body.hasOwnProperty('CertIssuer')){
            if(event.body.CertIssuer.length == 0){
                // nothing to do
                CertIssuer = '';
            } else {
                CertIssuer = Trim(event.body.CertIssuer);
            }

        }
        if(event.body.hasOwnProperty('CertNumber')){
            if(event.body.CertNumber.length == 0){
                // nothing to do
                CertNumber = '';
            } else {
                CertNumber = Trim(event.body.CertNumber);
            }
        }
        if(event.body.hasOwnProperty('CertURL')){
            if(event.body.CertURL.length == 0){
                // nothing to do
                CertURL = '';
            } else {
                CertURL = Trim(event.body.CertURL);
            }
        }
        wf.user_exp_item = {
            "UserID": wf.user_sub_id,
            "ItemID": wf.ItemID,
            "CertName": Trim(event.body.CertName),
            "CertExpiryFlag": event.body.CertExpiryFlag,
            "Description": Description,
            "CertDate": CertDate,
            "Status": "Active",
            "CertIssuer":CertIssuer,
            "CertExpiryDate":CertExpiryDate,
            "CertLinks":CertLinks,
            "CertNumber":CertNumber,
            "CertURL":CertURL,
            "TSCreated": Created_Timestamp,
            "TSUpdated": Created_Timestamp
        };
        if(event.body.hasOwnProperty('Description')){
            if(event.body.Description.length == 0){
                // nothing to do
                delete wf.user_exp_item["Description"];
            }
        } else {
            delete wf.user_exp_item["Description"];
        }
        if(event.body.hasOwnProperty('CertDate')){
            if(event.body.CertDate.length == 0){
                // nothing to do
                delete wf.user_exp_item["CertDate"];
            }
        } else {
            delete wf.user_exp_item["CertDate"];
        }
        if(event.body.hasOwnProperty('CertIssuer')){
            if(event.body.CertIssuer.length == 0){
                // nothing to do
                delete wf.user_exp_item["CertIssuer"];
            }
        } else {
            delete wf.user_exp_item["CertIssuer"];
        }
        if(event.body.hasOwnProperty('CertExpiryDate')){
            if(event.body.CertExpiryFlag == 'No'){
                delete wf.user_exp_item["CertExpiryDate"];
            } else {
                if(event.body.CertExpiryFlag != 'Yes'){
                    delete wf.user_exp_item["CertExpiryDate"];
                }
            }
        } else {
            delete wf.user_exp_item["CertExpiryDate"];
        }
        if(event.body.hasOwnProperty('CertLinks')){
            if(event.body.CertLinks.length == 0){
                // nothing to do
                delete wf.user_exp_item["CertLinks"];
            }
        } else {
            delete wf.user_exp_item["CertLinks"];
        }
        if(event.body.hasOwnProperty('CertNumber')){
            if(event.body.CertNumber.length == 0){
                // nothing to do
                delete wf.user_exp_item["CertNumber"];
            }
        } else {
            delete wf.user_exp_item["CertNumber"];
        }
        if(event.body.hasOwnProperty('CertURL')){
            if(event.body.CertURL.length == 0){
                // nothing to do
                delete wf.user_exp_item["CertURL"];
            }
        } else {
            delete wf.user_exp_item["CertURL"];
        }
        var params = {
            Item: wf.user_exp_item,
            TableName: TableName
        };
        docClient.put(params, function(err, data) {
            if (err) {
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                        "code": 500,
                        "message": "Internal server error",
                        "type": "Server Error",
                        "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                if(!wf.user_exp_item["CertDate"]){
                    wf.user_exp_item["CertDate"] ="";
                }
                if(!wf.user_exp_item["CertExpiryFlag"]){
                    wf.user_exp_item["CertExpiryFlag"] ="No";
                }
                if(!wf.user_exp_item["CertExpiryDate"]){
                    wf.user_exp_item["CertExpiryDate"] ="";
                }
                if(!wf.user_exp_item["CertIssuer"]){
                    wf.user_exp_item["CertIssuer"] ="";
                }
                if(!wf.user_exp_item["CertLinks"]){
                    wf.user_exp_item["CertLinks"] = [];
                }
                if(!wf.user_exp_item["CertNumber"]){
                    wf.user_exp_item["CertNumber"] ="";
                }
                if(!wf.user_exp_item["CertURL"]){
                    wf.user_exp_item["CertURL"] ="";
                }
                if(!wf.user_exp_item["Description"]){
                    wf.user_exp_item["Description"] ="";
                }
                context.done(null,{
                    "data":{
                        "MainData": wf.user_exp_item
                    },
                    "error": null,
                    "statusCode": 201
                });
                return;
            }

        });


    })

    wf.emit('check_request_body');
};

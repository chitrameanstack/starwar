var AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({
    "region": process.env.AwsRegion
});
const TableName = process.env.TableName;

exports.handler = (event, context, callback) => {

    var EventEmitter = require('events').EventEmitter;
    var wf = new EventEmitter();

    if (event.userID.length == 0) {
        context.fail(JSON.stringify({
            "data":null,
            "error": {
                "code": 400,
                "message": "Missing parameters /userID/",
                "type": "Missing parameters",
                "should_display_error": "false"
            },
            "statusCode": 400
        }));
        return;
    }
    wf.user_sub_id = event.userID;

    function Trim(string) {
        string = string.replace(/ {1,}/g," ");
        return string.trim();
    }

    function TrimArrayElements(array){
        for(var i=0;i<array.length;i++){
            array[i] = Trim(array[i]);
        }
        return array;
    }

    wf.once('check_request_body', function() {
        if(!event.body.hasOwnProperty("Hobbies")){
            context.fail(JSON.stringify({
                "data":null,
                "error": {
                    "code": 400,
                    "message": "Missing parameters /Hobbies/",
                    "type": "Missing parameters",
                    "should_display_error": "false"
                },
                "statusCode": 400
            }));
            return;
        } else {
            if(event.body.Hobbies.length == 0){
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                        "code": 400,
                        "message": "Missing parameters /Hobby/",
                        "type": "Missing parameters",
                        "should_display_error": "false"
                    },
                    "statusCode": 400
                }));
                return;
            } else {
                for(var i=0;i<event.body.Hobbies.length;i++){
                    var errors = [];
                    var missingParameters = '';
                    if (!event.body.Hobbies[i].hasOwnProperty("Hobby")) {
                        errors.push("Hobby");
                        missingParameters = '/' + missingParameters + 'Hobby/';
                    }
                    if (errors.length > 0) {
                        var message = 'Missing parameters ' + missingParameters;
                        context.fail(JSON.stringify({
                            "data": null,
                            "error": {
                                "code": 400,
                                "message": message,
                                "type": "Missing parameters",
                                "should_display_error": "false"
                            },
                            "statusCode": 400
                        }));
                        return;
                    }
                    if(i == event.body.Hobbies.length-1){
                        wf.emit('check_request_by_field_level');
                    }
                }
            }
        }
    });
    wf.once('check_request_by_field_level',function(){
        for(var i=0;i<event.body.Hobbies.length;i++){
            var errors = [];
            var invalidParameters = '';
            if (event.body.Hobbies[i].Hobby.length == 0) {
                errors.push("Hobby");
                invalidParameters = '/' + invalidParameters + 'Hobby/';
            }
            if (errors.length > 0) {
                var message = 'Invalid parameters ' + invalidParameters;
                context.fail(JSON.stringify({
                    "data": null,
                    "error": {
                        "code": 400,
                        "message": message,
                        "type": "Missing parameters",
                        "should_display_error": "false"
                    },
                    "statusCode": 400
                }));
                return;
            }
            if(i == event.body.Hobbies.length-1){
                wf.emit('get_user_item_count');
            }
        }
    })

    wf.once('get_user_item_count', function (){
        var params = {
            TableName: TableName,
            KeyConditionExpression: "#uid = :userId",
            ExpressionAttributeNames: {
                "#uid": "UserID"
            },
            ExpressionAttributeValues: {
                ":userId": wf.user_sub_id
            },
            ProjectionExpression: ["ItemID"]
        };

        docClient.query(params, function(err, data) {
            if (err) {
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                        "code": 500,
                        "message": "Internal server error",
                        "type": "Server Error",
                        "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                if(data.Items.length == 0){
                    wf.ItemID = 1;
                    wf.emit('store_data_to_user_Hobby_table');
                } else {
                    var maxItem = Math.max.apply(Math, data.Items.map(function(o) { return o.ItemID; }));
                    wf.ItemID = maxItem+1;
                    wf.emit('store_data_to_user_Hobby_table');
                }
            }
        });
    })

    wf.once('store_data_to_user_Hobby_table',function(){
        wf.HobbyList = [];
        var Created_Timestamp = new Date().toISOString();
        var i=0;
        function addHobby(i){
            if(i==event.body.Hobbies.length){
                context.done(null,{
                    "data":{
                        "MainData": wf.HobbyList
                    },
                    "error": null,
                    "statusCode": 201
                });
                return;
            } else {
                wf.user_Hobby_item = {
                    "UserID": wf.user_sub_id,
                    "ItemID": wf.ItemID,
                    "Hobby": Trim(event.body.Hobbies[i].Hobby),
                    "Status": "Active",
                    "TSCreated": Created_Timestamp,
                    "TSUpdated": Created_Timestamp
                };
                if(event.body.Hobbies[i].hasOwnProperty('Description')){
                    if(event.body.Hobbies[i].Description.length != 0){
                        wf.user_Hobby_item.Description = Trim(event.body.Hobbies[i].Description);
                    }
                }
                var params = {
                    Item: wf.user_Hobby_item,
                    TableName: TableName
                };
                docClient.put(params, function(err, data) {
                    if (err) {
                        context.fail(JSON.stringify({
                            "data":null,
                            "error": {
                                "code": 500,
                                "message": "Internal server error",
                                "type": "Server Error",
                                "should_display_error": "false"
                            },
                            "statusCode": 500
                        }));
                        return;
                    } else {
                        if(!wf.user_Hobby_item["Description"]){
                            wf.user_Hobby_item["Description"] ="";
                        }
                        wf.HobbyList.push(wf.user_Hobby_item);
                        i++;wf.ItemID++;
                        addHobby(i);
                    }

                });
            }

        }
        addHobby(i);
    })

    wf.emit('check_request_body');
};

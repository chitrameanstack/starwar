var AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({
    "region": process.env.AwsRegion
});
const TableName = process.env.TableName;

exports.handler = (event, context, callback) => {

    var EventEmitter = require('events').EventEmitter;
    var wf = new EventEmitter();

    if (event.userID.length == 0) {
        context.fail(JSON.stringify({
            "data":null,
            "error": {
              "code": 400,
              "message": "Missing parameters /userID/",
              "type": "Missing parameters",
              "should_display_error": "false"
            },
            "statusCode": 400
        }));
        return;var AWS = require("aws-sdk");
        const docClient = new AWS.DynamoDB.DocumentClient({
            "region": process.env.AwsRegion
        });
        const TableName = process.env.TableName;

        exports.handler = (event, context, callback) => {

            var EventEmitter = require('events').EventEmitter;
            var wf = new EventEmitter();

            if (event.userID.length == 0) {
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                        "code": 400,
                        "message": "Missing parameters /userID/",
                        "type": "Missing parameters",
                        "should_display_error": "false"
                    },
                    "statusCode": 400
                }));
                return;
            }
            wf.user_sub_id = event.userID;

            function Trim(string) {
                string = string.replace(/ {1,}/g," ");
                return string.trim();
            }

            function TrimArrayElements(array){
                for(var i=0;i<array.length;i++){
                    array[i] = Trim(array[i]);
                }
                return array;
            }

            wf.once('check_request_body', function() {
                if(!event.body.hasOwnProperty("Languages")){
                    context.fail(JSON.stringify({
                        "data":null,
                        "error": {
                            "code": 400,
                            "message": "Missing parameters /Languages/",
                            "type": "Missing parameters",
                            "should_display_error": "false"
                        },
                        "statusCode": 400
                    }));
                    return;
                } else {
                    if(event.body.Languages.length == 0){
                        context.fail(JSON.stringify({
                            "data":null,
                            "error": {
                                "code": 400,
                                "message": "Missing parameters /Language/",
                                "type": "Missing parameters",
                                "should_display_error": "false"
                            },
                            "statusCode": 400
                        }));
                        return;
                    } else {
                        for(var i=0;i<event.body.Languages.length;i++){
                            var errors = [];
                            var missingParameters = '';
                            if (!event.body.Languages[i].hasOwnProperty("Language")) {
                                errors.push("Language");
                                missingParameters = '/' + missingParameters + 'Language/';
                            }
                            if (errors.length > 0) {
                                var message = 'Missing parameters ' + missingParameters;
                                context.fail(JSON.stringify({
                                    "data": null,
                                    "error": {
                                        "code": 400,
                                        "message": message,
                                        "type": "Missing parameters",
                                        "should_display_error": "false"
                                    },
                                    "statusCode": 400
                                }));
                                return;
                            }
                            if(i == event.body.Languages.length-1){
                                wf.emit('check_request_by_field_level');
                            }
                        }
                    }
                }
            });
            wf.once('check_request_by_field_level',function(){
                for(var i=0;i<event.body.Languages.length;i++){
                    var errors = [];
                    var invalidParameters = '';
                    if (event.body.Languages[i].Language.length == 0) {
                        errors.push("Language");
                        invalidParameters = '/' + invalidParameters + 'Language/';
                    }
                    if (errors.length > 0) {
                        var message = 'Invalid parameters ' + invalidParameters;
                        context.fail(JSON.stringify({
                            "data": null,
                            "error": {
                                "code": 400,
                                "message": message,
                                "type": "Missing parameters",
                                "should_display_error": "false"
                            },
                            "statusCode": 400
                        }));
                        return;
                    }
                    if(i == event.body.Languages.length-1){
                        wf.emit('get_user_item_count');
                    }
                }
            })

            wf.once('get_user_item_count', function (){
                var params = {
                    TableName: TableName,
                    KeyConditionExpression: "#uid = :userId",
                    ExpressionAttributeNames: {
                        "#uid": "UserID"
                    },
                    ExpressionAttributeValues: {
                        ":userId": wf.user_sub_id
                    },
                    ProjectionExpression: ["ItemID"]
                };

                docClient.query(params, function(err, data) {
                    if (err) {
                        context.fail(JSON.stringify({
                            "data":null,
                            "error": {
                                "code": 500,
                                "message": "Internal server error",
                                "type": "Server Error",
                                "should_display_error": "false"
                            },
                            "statusCode": 500
                        }));
                        return;
                    } else {
                        if(data.Items.length == 0){
                            wf.ItemID = 1;
                            wf.emit('store_data_to_user_Language_table');
                        } else {
                            var maxItem = Math.max.apply(Math, data.Items.map(function(o) { return o.ItemID; }));
                            wf.ItemID = maxItem+1;
                            wf.emit('store_data_to_user_Language_table');
                        }
                    }
                });
            })

            wf.once('store_data_to_user_Language_table',function(){
                wf.LanguageList = [];
                var Created_Timestamp = new Date().toISOString();
                var i=0;
                function addLanguage(i){
                    if(i==event.body.Languages.length){
                        context.done(null,{
                            "data":{
                                "MainData": wf.LanguageList
                            },
                            "error": null,
                            "statusCode": 201
                        });
                        return;
                    } else {
                        wf.user_Language_item = {
                            "UserID": wf.user_sub_id,
                            "ItemID": wf.ItemID,
                            "Language": Trim(event.body.Languages[i].Language),
                            "Status": "Active",
                            "TSCreated": Created_Timestamp,
                            "TSUpdated": Created_Timestamp
                        };
                        if(event.body.Languages[i].hasOwnProperty('Read')){
                            if(event.body.Languages[i].Read.length != 0){
                                if(event.body.Languages[i].Read != 'Yes' && event.body.Languages[i].Read != 'No'){
                                    context.fail(JSON.stringify({
                                        "data": null,
                                        "error": {
                                            "code": 400,
                                            "message": 'Invalid parameters /Read/',
                                            "type": "Missing parameters",
                                            "should_display_error": "false"
                                        },
                                        "statusCode": 400
                                    }));
                                    return;
                                } else {
                                    wf.user_Language_item.Read = event.body.Languages[i].Read;
                                }
                            }
                        }
                        if(event.body.Languages[i].hasOwnProperty('Write')){
                            if(event.body.Languages[i].Write.length != 0){
                                if(event.body.Languages[i].Write != 'Yes' && event.body.Languages[i].Write != 'No'){
                                    context.fail(JSON.stringify({
                                        "data": null,
                                        "error": {
                                            "code": 400,
                                            "message": 'Invalid parameters /Write/',
                                            "type": "Missing parameters",
                                            "should_display_error": "false"
                                        },
                                        "statusCode": 400
                                    }));
                                    return;
                                } else {
                                    wf.user_Language_item.Write = event.body.Languages[i].Write;
                                }
                            }
                        }
                        if(event.body.Languages[i].hasOwnProperty('Speak')){
                            if(event.body.Languages[i].Speak.length != 0){
                                if(event.body.Languages[i].Speak != 'Yes' && event.body.Languages[i].Speak != 'No'){
                                    context.fail(JSON.stringify({
                                        "data": null,
                                        "error": {
                                            "code": 400,
                                            "message": 'Invalid parameters /Speak/',
                                            "type": "Missing parameters",
                                            "should_display_error": "false"
                                        },
                                        "statusCode": 400
                                    }));
                                    return;
                                } else {
                                    wf.user_Language_item.Speak = event.body.Languages[i].Speak;
                                }
                            }
                        }
                        var params = {
                            Item: wf.user_Language_item,
                            TableName: TableName
                        };
                        docClient.put(params, function(err, data) {
                            if (err) {
                                context.fail(JSON.stringify({
                                    "data":null,
                                    "error": {
                                        "code": 500,
                                        "message": "Internal server error",
                                        "type": "Server Error",
                                        "should_display_error": "false"
                                    },
                                    "statusCode": 500
                                }));
                                return;
                            } else {
                                if(!wf.user_Language_item["Description"]){
                                    wf.user_Language_item["Description"] ="";
                                }
                                wf.LanguageList.push(wf.user_Language_item);
                                i++;wf.ItemID++;
                                addLanguage(i);
                            }

                        });
                    }

                }
                addLanguage(i);
            })

            wf.emit('check_request_body');
        };

    }
    wf.user_sub_id = event.userID;

    function Trim(string) {
        string = string.replace(/ {1,}/g," ");
        return string.trim();
    }

    function TrimArrayElements(array){
        for(var i=0;i<array.length;i++){
            array[i] = Trim(array[i]);
        }
        return array;
    }

    wf.once('check_request_body', function() {
        var errors = [];
        var missingParameters = '';
        if(!event.body.hasOwnProperty("Language")){
            errors.push("Language");
            missingParameters = '/'+missingParameters+'Language/';
        }
        if(errors.length > 0){
            var message = 'Missing parameters '+missingParameters;
            context.fail(JSON.stringify({
                "data":null,
                "error": {
                  "code": 400,
                  "message": message,
                  "type": "Missing parameters",
                  "should_display_error": "false"
                },
                "statusCode": 400
            }));
            return;
        } else {
            wf.emit('check_request_by_field_level');
             
        }
    });
    wf.once('check_request_by_field_level',function(){
      var Language,Read;
      Language = event.body.Language;

      var errors = [];
      var invalidParameters = '';
      if(Language.length == 0){
          errors.push("Language");
          invalidParameters = '/'+invalidParameters+'Language/';
      }
      if(errors.length > 0){
          var message = 'Invalid parameters '+invalidParameters;
          context.fail(JSON.stringify({
              "data":null,
              "error": {
                "code": 400,
                "message": message,
                "type": "Invalid parameters",
                "should_display_error": "false"
              },
              "statusCode": 400
          }));
          return;
      } else {
          wf.emit('get_user_item_count');
      }
    })

    wf.once('get_user_item_count', function (){
      var params = {
            TableName: TableName,
            KeyConditionExpression: "#uid = :userId",
            ExpressionAttributeNames: {
                "#uid": "UserID"
            },
            ExpressionAttributeValues: {
                ":userId": wf.user_sub_id
            },
            ProjectionExpression: ["ItemID"]
        };

        docClient.query(params, function(err, data) {
            if (err) {
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                      "code": 500,
                      "message": "Internal server error",
                      "type": "Server Error",
                      "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                if(data.Items.length == 0){
                  wf.ItemID = 1;
                  wf.emit('store_data_to_user_language_table');
                } else {
                  var maxItem = Math.max.apply(Math, data.Items.map(function(o) { return o.ItemID; }));
                  wf.ItemID = maxItem+1;
                  wf.emit('store_data_to_user_language_table');
                }
            }
        });
    })

    wf.once('store_data_to_user_language_table',function(){
      var Read,Speak,Write;
      var Created_Timestamp = new Date().toISOString();
      if(event.body.hasOwnProperty('Read')){
          if(event.body.Read.length == 0){
              // nothing to do
              Read = '';
          } else {          
              Read = Trim(event.body.Read);
          }
      } 
      if(event.body.hasOwnProperty('Write')){
          if(event.body.Write.length == 0){
              // nothing to do
              Write = '';
          } else {          
              Write = Trim(event.body.Write);
          }
      } 
      if(event.body.hasOwnProperty('Speak')){
          if(event.body.Speak.length == 0){
              // nothing to do
              Speak = '';
          } else {          
              Speak = Trim(event.body.Speak);
          }
      } 
      wf.user_language_item = {
          "UserID": wf.user_sub_id,
          "ItemID": wf.ItemID,
          "Language": Trim(event.body.Language),
          "Status": "Active",
          "Read": Read,
          "Write": Write,
          "Speak": Speak,
          "TSCreated": Created_Timestamp,
          "TSUpdated": Created_Timestamp
      };
      if(event.body.hasOwnProperty('Read')){
          if(event.body.Read.length == 0){
              // nothing to do
              delete wf.user_language_item["Read"];
          } 
      } else {
        delete wf.user_language_item["Read"];
      }
      if(event.body.hasOwnProperty('Write')){
          if(event.body.Write.length == 0){
              // nothing to do
              delete wf.user_language_item["Write"];
          } 
      } else {
        delete wf.user_language_item["Write"];
      }
      if(event.body.hasOwnProperty('Speak')){
          if(event.body.Speak.length == 0){
              // nothing to do
              delete wf.user_language_item["Speak"];
          } 
      } else {
        delete wf.user_language_item["Speak"];
      }
      var params = {
          Item: wf.user_language_item,
          TableName: TableName
      };
      docClient.put(params, function(err, data) {
          if (err) {
              context.fail(JSON.stringify({
                  "data":null,
                  "error": {
                    "code": 500,
                    "message": "Internal server error",
                    "type": "Server Error",
                    "should_display_error": "false"
                  },
                  "statusCode": 500
              }));
              return;
          } else {
              if(!wf.user_language_item["Read"]){
                  wf.user_language_item["Read"] ="";
              }
              if(!wf.user_language_item["Write"]){
                  wf.user_language_item["Write"] ="";
              }
              if(!wf.user_language_item["Speak"]){
                  wf.user_language_item["Speak"] ="";
              }
              context.done(null,{
                  "data":{
                    "MainData": wf.user_language_item
                  },
                  "error": null,
                  "statusCode": 201
              });
              return;
          }

      });

      
    })

    wf.emit('check_request_body');
};

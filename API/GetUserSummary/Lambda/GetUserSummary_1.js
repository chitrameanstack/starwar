var AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({
    "region": process.env.AwsRegion
});
const TableName = process.env.TableName;

exports.handler = (event, context, callback) => {

    var EventEmitter = require('events').EventEmitter;
    var wf = new EventEmitter();

    if (event.userID.length == 0) {
        context.fail(JSON.stringify({
            "data":null,
            "error": {
              "code": 400,
              "message": "Missing parameters /userID/",
              "type": "Missing parameters",
              "should_display_error": "false"
            },
            "statusCode": 400
        }));
        return;
    }
    wf.user_sub_id = event.userID;

    wf.once('get_user_summary', function() {
        var params = {
            TableName: TableName,
            KeyConditionExpression: "#uid = :userId",
            ExpressionAttributeNames: {
                "#uid": "UserID"
            },
            ExpressionAttributeValues: {
                ":userId": wf.user_sub_id,
            },
            ProjectionExpression: ["UserID","Headline","Summary"]
        };

        docClient.query(params, function(err, data) {
            if (err) {
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                      "code": 500,
                      "message": "Internal server error",
                      "type": "Server Error",
                      "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                for(var i=0;i<data.Items.length;i++){
                    if(!data.Items[i]["Summary"]){
                        data.Items[i]["Summary"] ="";
                    }
                }
                context.done(null,{
                    "data":{
                      "MainData": data.Items 
                    },
                    "error": null,
                    "statusCode": 200
                });

            }
        });
    })
    wf.emit('get_user_summary');
};
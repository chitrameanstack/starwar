var AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({
    "region": process.env.AwsRegion
});
const TableName = process.env.TableName;

exports.handler = (event, context, callback) => {

    var EventEmitter = require('events').EventEmitter;
    var wf = new EventEmitter();

    wf.once('check_request_by_field_level',function(){
        var UserID,ItemID;
        UserID = event.userID;
        ItemID = event.itemID;
        var errors = [];
        var missingParameters = '';
        if(UserID.length == 0){
          errors.push("UserID");
          missingParameters = '/'+missingParameters+'UserID/';
        }
        if(ItemID.length == 0 || isNaN(ItemID)){
            if(errors.length>0){
                missingParameters = missingParameters+'ItemID/';
            } else {
                missingParameters = '/'+missingParameters+'ItemID/';
            }
            errors.push("ItemID");
        }
        if(errors.length > 0){
            var message = 'Missing/Invalid parameters '+missingParameters;
            context.fail(JSON.stringify({
                "data":null,
                "error": {
                  "code": 400,
                  "message": message,
                  "type": "Missing/Invalid parameters",
                  "should_display_error": "false"
                },
                "statusCode": 400
            }));
            return;
        } else {
          wf.emit('get_user_vlntr_experience_item');
        }
    })

    wf.once('get_user_vlntr_experience_item', function() {
        wf.user_sub_id = event.userID;
        wf.ItemID = parseInt(event.itemID);
        var params = {
            TableName: TableName,
            KeyConditionExpression: "#uid = :userId AND #itemID = :ItemID" ,
            FilterExpression: '#sts = :sts',
            ExpressionAttributeNames: {
                "#uid": "UserID",
                "#itemID": "ItemID",
                "#sts": "Status"
            },
            ExpressionAttributeValues: {
                ":userId": wf.user_sub_id,
                ":ItemID":wf.ItemID,
                ":sts":"Active"
            },
            ProjectionExpression: ["UserID","ItemID","OrgName","Description","CurrentFlag","VolunteerCause","VolunteerRole","LocationCity","LocationCountry","VolunteerExpDateFrom","VolunteerExpLinks","VolunteerExpDateTo"]
        };

        docClient.query(params, function(err, data) {
            if (err) {
              console.log(err);
                context.fail(JSON.stringify({
                    "data":null,
                    "error": {
                      "code": 500,
                      "message": "Internal server error",
                      "type": "Server Error",
                      "should_display_error": "false"
                    },
                    "statusCode": 500
                }));
                return;
            } else {
                for(var i=0;i<data.Items.length;i++){
                    if(!data.Items[i]["Description"]){
                        data.Items[i]["Description"] ="";
                    }
                    if(!data.Items[i]["VolunteerExpDateTo"]){
                        data.Items[i]["VolunteerExpDateTo"] ="";
                    }
                    if(!data.Items[i]["VolunteerExpLinks"]){
                        data.Items[i]["VolunteerExpLinks"] = [];
                    }
                }
                context.done(null,{
                    "data":{
                      "MainData": data.Items 
                    },
                    "error": null,
                    "statusCode": 200
                });

            }
        });
    })

    wf.emit('check_request_by_field_level');
};